Name:           plexus-ant-factory
Version:        1.0~alpha.2.1
Release:        2%{?dist}
Summary:        Plexus Ant component factory
# Email from copyright holder confirms license.
# See plexus-ant-factory_license_and_copyright.txt
License:        ASL 2.0
URL:            http://plexus.codehaus.org/
BuildArch:      noarch

Source0:        %{name}-src.tar.bz2
# svn export http://svn.codehaus.org/plexus/tags/plexus-ant-factory-1.0-alpha-2.1/ plexus-ant-factory/
# tar cjf plexus-ant-factory-src.tar.bz2 plexus-ant-factory/
Source1:        http://www.apache.org/licenses/LICENSE-2.0.txt

BuildRequires:  maven-local
BuildRequires:  mvn(org.apache.ant:ant)
BuildRequires:  mvn(org.apache.ant:ant-launcher)
BuildRequires:  mvn(org.codehaus.plexus:plexus-component-factories:pom:)
BuildRequires:  mvn(org.codehaus.plexus:plexus-container-default)
BuildRequires:  mvn(org.codehaus.plexus:plexus-utils)

%description
Ant component class creator for Plexus.

%package javadoc
Summary:        Javadoc for %{name}

%description javadoc
Javadoc for %{name}.

%prep
%setup -q -n %{name}
cp %{SOURCE1} LICENSE

%build
%mvn_file  : plexus/ant-factory
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc LICENSE

%files javadoc -f .mfiles-javadoc
%doc LICENSE

%changelog
* Tue Nov 05 2019 Mikolaj Izdebski <mizdebsk@redhat.com> - 1.0~alpha.2.1-2
- Mass rebuild for javapackages-tools 201902

* Wed Sep 18 2019 Marian Koncek <mkoncek@redhat.com> - 1.0~alpha.2.1-1
- Use tilde versioning

* Fri May 24 2019 Mikolaj Izdebski <mizdebsk@redhat.com> - 1.0-0.21.a2.2
- Mass rebuild for javapackages-tools 201901

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0:1.0-0.20.a2.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0:1.0-0.19.a2.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0:1.0-0.18.a2.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0:1.0-0.17.a2.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0:1.0-0.16.a2.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Mon Aug  4 2014 Mikolaj Izdebski <mizdebsk@redhat.com> - 0:1.0-0.15.a2.2
- Fix build-requires on plexus-classworlds

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0:1.0-0.15.a2.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Tue Mar 04 2014 Stanislav Ochotnicky <sochotnicky@redhat.com> - 0:1.0-0.14.a2.1
- Use Requires: java-headless rebuild (#1067528)

* Sat Aug 24 2013 Mat Booth <fedora@matbooth.co.uk> - 0:1.0-0.13.a2.1
- Remove unneeded BRs

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0:1.0-0.12.a2.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Wed Feb 06 2013 Java SIG <java-devel@lists.fedoraproject.org> - 0:1.0-0.11.a2.1
- Update for https://fedoraproject.org/wiki/Fedora_19_Maven_Rebuild
- Replace maven BuildRequires with maven-local

* Thu Jan 17 2013 Michal Srb <msrb@redhat.com> - 0:1.0-0.10.a2.1
- Build with xmvn
- Added BR on plexus-component-factories-pom

* Mon Dec 10 2012 Michal Srb <msrb@redhat.com> - 0:1.0-0.9.a2.1
- migrated to plexus-containers-container-default (#878580)
- using global is preferred over define when defining macros
- removed not used custom depmap file and its occurrence in spec file
- removed unused Ant buildfile
- fixed release number

* Mon Nov 12 2012 Mikolaj Izdebski <mizdebsk@redhat.com> - 0:1.0-0.8.a2.1.4
- Add LICENSE-2.0.txt to sources

* Fri Nov  9 2012 Mikolaj Izdebski <mizdebsk@redhat.com> - 0:1.0-0.8.a2.1.3
- Install ASL 2.0 license file

* Fri Jul  27 2012 Mikolaj Izdebski <mizdebsk@redhat.com> - 0:1.0-0.8.a2.1.2
- Update to maven 3
- Replace nonstandard groups names with standard ones
- Drop support for non-maven build
- Add workarounds for RPM bug
- Cleanup according to Fedora Packaging Guidelines
- Include missing copyright file in javadoc package

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0:1.0-0.7.a2.1.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0:1.0-0.6.a2.1.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0:1.0-0.5.a2.1.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Dec 23 2009 Alexander Kurtakov <akurtako@redhat.com> 0:1.0-0.4.a2.1.2
- BR maven-doxia-sitetools.

* Wed Dec 23 2009 Alexander Kurtakov <akurtako@redhat.com> 0:1.0-0.4.a2.1.1
- Update to 1.0 alpha 2.1.
- Drop gcj_support.

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0:1.0-0.4.a1.1.11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sat Feb 28 2009 Deepak Bhole <dbhole@redhat.com> 1.0-0.3.a1.1.11
- Build with maven

* Sat Feb 28 2009 Deepak Bhole <dbhole@redhat.com> - 0:1.0-0.3.a1.1.10
- Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0:1.0-0.3.a1.1.9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Aug 13 2008 Deepak Bhole <dbhole@redhat.com> 1.0-0.2.a1.1.9
- Build for ppc64

* Wed Jul  9 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1.0-0.2.a1.1.8
- drop repotag

* Thu May 29 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.0-0.2.a1.1jpp.7
- include proof of license
- fix license tag

* Thu Feb 28 2008 Deepak Bhole <dbhole@redhat.com> 1.0-0.2.a1.1jpp.6
- Rebuild

* Fri Sep 21 2007 Deepak Bhole <dbhole@redhat.com> 1.0-0.1.a1.2jpp.5
- ExcludeArch ppc64

* Mon Sep 10 2007 Deepak Bhole <dbhole@redhat.com> 1.0-0.1.a1.2jpp.4
- Build with maven

* Fri Aug 31 2007 Deepak Bhole <dbhole@redhat.com> 1.0-0.1.a1.2jpp.3
- Build without maven (to build on ppc)

* Tue Mar 20 2007 Deepak Bhole <dbhole@redhat.com> 1.0-0.1.a1.2jpp.2
- Build with maven

* Fri Feb 23 2007 Tania Bento <tbento@redhat.com> 0:1.0-0.1.a1.2jpp.1
- Fixed %%Release.
- Fixed %%BuildRoot.
- Removed %%Vendor.
- Removed %%Distribution.
- Removed %%post and %%postun sections for javadoc.
- Defined _with_gcj_supoprt and _gcj_support.
- Changed to use cp -p to preserve timestamps.

* Tue Oct 17 2006 Deepak Bhole <dbhole@redhat.com> 1.0-0.a1.2jpp
- Update for maven2 9jpp.

* Thu Sep 07 2006 Deepak Bhole <dbhole@redhat.com> 1.0-0.a1.1jpp
- Initial build
